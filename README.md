**Gallery Site Script Free**

Setup and manage your own online image gallery. 

**About Gallery Site Script Free**

This site script can be used to create an online image and photo gallery.

This is a community based release that is supplied without support or liability. You are free to use the fonts site script as you wish. The only restriction to this PHP script is that this README file must not be edited or deleted. 

**Features:**

* Completely FREE!
* Written in php.
* All pages are created using Apaches mod-rewrite so it looks like flat html pages to the search engines.
* Automatic creation of the thumbnail images.
* Ability to categorise into albums.
* No database is needed. Simply upload your images into their own folders (albums) and the script will do the rest.
* Images are automatically titled with the original filename.
* JPG, PNG and static GIF images supported.
* Single config file allows for amending thumbnail size, how many per page, site title, colours and more.
* Automatic page titles/meta information based on the image filename/directory.

**Requirements**

* php 4.x
* mod rewrite module on apache
* gd library
* No real HD requirements, this will obviously depend on how many images you have.

**Installation**

Full instructions are given in the _setup-instructions.txt file found within the repository.

**Contributions**

If you'd like to contribute to the project, please contact us via the project https://bitbucket.org/MFScripts/gallery-site-script

**License**

Gallery Site Script Free is copyrighted by http://mfscripts.com and is released under the http://opensource.org/licenses/MIT. You are free to use the gallery site script as you wish. The only restriction to this PHP script is that this README file must not be edited or deleted. 

**Support**

This code is released without any support and as-is. Any support requests on this version will be removed. For community driven support please use the project https://bitbucket.org/MFScripts/gallery-site-script